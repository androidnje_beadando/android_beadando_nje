package hu.nje.moviebrowsernje;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, "Moviedata.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase DB) {
        DB.execSQL("create Table Moviedetails(title TEXT primary key, actor TEXT, director TEXT)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase DB, int i, int ii) {
        DB.execSQL("drop Table if exists Moviedetails");
    }
    public Boolean insertmoviedata(String title, String actor, String director)
    {
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("actor", actor);
        contentValues.put("director", director);
        long result=DB.insert("Moviedetails", null, contentValues);
        if(result==-1){
            return false;
        }else{
            return true;
        }
    }
    public Boolean updatemoviedata(String title, String actor, String director)
    {
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("actor", actor);
        contentValues.put("director", director);
        Cursor cursor = DB.rawQuery("Select * from Moviedetails where title = ?", new String[]{title});
        if (cursor.getCount() > 0) {
            long result = DB.update("Moviedetails", contentValues, "name=?", new String[]{title});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    public Boolean deletedata (String title)
    {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Moviedetails where title = ?", new String[]{title});
        if (cursor.getCount() > 0) {
            long result = DB.delete("Moviedetails", "title=?", new String[]{title});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public Cursor getdata ()
    {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Moviedetails", null);
        return cursor;
    }
}