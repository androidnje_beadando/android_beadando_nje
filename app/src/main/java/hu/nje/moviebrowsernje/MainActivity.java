package hu.nje.moviebrowsernje;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText title, actor, director;
    Button insert, update, delete, view;
    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        title = findViewById(R.id.title);
        actor = findViewById(R.id.actor);
        director = findViewById(R.id.director);
        insert = findViewById(R.id.btnInsert);
        update = findViewById(R.id.btnUpdate);
        delete = findViewById(R.id.btnDelete);
        view = findViewById(R.id.btnView);
        DB = new DBHelper(this);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titleTXT = title.getText().toString();
                String actorTXT = actor.getText().toString();
                String directorTXT = director.getText().toString();

                Boolean checkinsertdata = DB.insertmoviedata(titleTXT, actorTXT, directorTXT);
                if(checkinsertdata==true)
                    Toast.makeText(MainActivity.this, "New Movie Inserted", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this, "Failed to insert", Toast.LENGTH_SHORT).show();
            }        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titleTXT = title.getText().toString();
                String actorTXT = actor.getText().toString();
                String directorTXT = director.getText().toString();

                Boolean checkupdatedata = DB.updatemoviedata(titleTXT, actorTXT, directorTXT);
                if(checkupdatedata==true)
                    Toast.makeText(MainActivity.this, "Movie Updated", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this, "Failed to update", Toast.LENGTH_SHORT).show();
            }        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titleTXT = title.getText().toString();
                Boolean checkdeletedata = DB.deletedata(titleTXT);
                if(checkdeletedata==true)
                    Toast.makeText(MainActivity.this, "Movie Deleted", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this, "Failed to delete", Toast.LENGTH_SHORT).show();
            }        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor res = DB.getdata();
                if(res.getCount()==0){
                    Toast.makeText(MainActivity.this, "No movie in the DB", Toast.LENGTH_SHORT).show();
                    return;
                }
                StringBuffer buffer = new StringBuffer();
                while(res.moveToNext()){
                    buffer.append("Title:"+res.getString(0)+"\n");
                    buffer.append("Actor:"+res.getString(1)+"\n");
                    buffer.append("Director:"+res.getString(2)+"\n\n");
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Movies");
                builder.setMessage(buffer.toString());
                builder.show();
            }        });

        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case
                    R.id.viewdb:
                Toast.makeText(this, "You clicked the ViewDB menu", Toast.LENGTH_SHORT).show();
                break;
            case
                    R.id.restapi:
                Toast.makeText(this, "You clicked the RestAPI menu", Toast.LENGTH_SHORT).show();
                break;
            case
                    R.id.about:
                Toast.makeText(this, "You clicked the About menu", Toast.LENGTH_SHORT).show();
                break;
            default:

                break;
        }
        return true;
    }
}
